const { join } = require("path");
const {
  readFileContents,
  writeFileContents
} = require("./handlers/fileHandler");
const axios = require("axios");
const ejs = require("ejs");

function runUpdate() {
  return new Promise(async (resolve, reject) => {
    try {
      let packagePath = join(__dirname, "package.json");
      let packageContents = await readFileContents(packagePath);
      let packageJson = JSON.parse(packageContents);
      let { version } = packageJson;

      let tokens = version.split(".");
      let oldVersionNumber = parseInt(tokens[2]) - 1;
      let oldVersion = tokens[0] + "." + tokens[1] + "." + oldVersionNumber;

      let aslServerDockerFileEjsPath = join(
        __dirname,
        "ejs_templates",
        "aslServer.DockerFile.ejs"
      );
      let aslServerDockerFileEjsContents = await readFileContents(
        aslServerDockerFileEjsPath
      );

      let template = ejs.compile(aslServerDockerFileEjsContents);
      let aslServerDockerFileContents = template({
        version: { new: version, old: "1.0.37" }
      });

      let aslServerDockerFilePath = join(
        __dirname,
        "dockerFiles",
        "aslServer.dockerFile"
      );

      await writeFileContents(
        aslServerDockerFilePath,
        aslServerDockerFileContents
      );

      console.log("UPGRADING DOCKER IMAGE TO ------------>" + version);
      let url =
        "http://localhost:8080/job/PUSH_LATEST_IMAGE/buildWithParameters?token=BUILD_TOKEN&NEW_VERSION=" +
        version;
      console.log(url);
      await axios.get(url);
      //console.log(aslServerDockerFileContents);
      return resolve(true);
    } catch (cause) {
      reject(cause);
    }
  });
}

runUpdate();
