const workflowHandler = require("../../handlers/workflowHandler");
const fileHandler = require("../../handlers/fileHandler");
const { join } = require("path");
const { ensureSame } = require("../../handlers/compareHandler");

describe("TESTING WORKFLOW HANDLER ...", function() {
  describe("getCardDesignFromConfig", function() {
    it("BASIC", async function() {
      let getCardDesignFromConfigBasic = await fileHandler.readFileContents(
        join(__dirname, "assets", "getCardDesignFromConfigBasicInput.json")
      );
      let { CardConfig } = JSON.parse(getCardDesignFromConfigBasic);
      let actualCardDesign = await workflowHandler.getCardDesignFromConfig(
        CardConfig
      );
      let expectedCardDesign = await fileHandler.readFileContents(
        join(__dirname, "assets", "getCardDesignFromConfigBasicOutput.txt")
      );
      await ensureSame(expectedCardDesign, actualCardDesign);
    });
  });
  describe("getWorkflowDesignFromConfig", function() {
    it("BASIC", async function() {
      let getWorkflowDesignBasicInput = await fileHandler.readFileContents(
        join(__dirname, "assets", "getWorkflowDesignBasicInput.json")
      );

      let { WorkflowName, WorkflowId, Menu } = JSON.parse(
        getWorkflowDesignBasicInput
      );

      let actualWorkflowDesign = await workflowHandler.getWorkflowDesign(
        WorkflowName,
        WorkflowId,
        Menu
      );

      let expectedWorkflowDesign = await fileHandler.readFileContents(
        join(__dirname, "assets", "getWorkflowDesignFromConfigBasicOutput.txt")
      );
      await ensureSame(expectedWorkflowDesign, actualWorkflowDesign);
    });
  });

  describe("generateExcel", function() {
    it("BASIC", async function() {
      let generateExcelBasicInput = await fileHandler.readFileContents(
        join(__dirname, "assets", "generateExcelBasicInput.json")
      );

      let { WorkflowName, WorkflowId, Menu } = JSON.parse(
        generateExcelBasicInput
      );

      let actualWorkflowDesign = await workflowHandler.generateExcel(
        WorkflowName,
        WorkflowId,
        Menu
      );

      let expectedWorkflowDesign = await fileHandler.readFileContents(
        join(__dirname, "assets", "generateExcelFromConfigBasicOutput.txt")
      );
      await ensureSame(expectedWorkflowDesign, actualWorkflowDesign);
    });
  });
});
