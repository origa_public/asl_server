const AdmZip = require("adm-zip");
const variableHandler = require("./variableHandler");
const path = require("path");
const pathHandler = require("./pathHandler");

exports.createZip = function (filesAbsolutePathArray, zipFileName) {
  return new Promise(async (resolve, reject) => {
    try {
      await variableHandler.checkIsValidArray(filesAbsolutePathArray);
      await variableHandler.checkIsValidString(zipFileName);

      let zip = new AdmZip();

      for (filePathIndex in filesAbsolutePathArray) {
        let filePath = filesAbsolutePathArray[filePathIndex];
        zip.addLocalFile(filePath);
      }

      let pathToExportZipDir = await pathHandler.getExportedZipDirPath();
      let pathToExportZipFile = path.join(
        pathToExportZipDir,
        zipFileName
      );
      zip.writeZip(pathToExportZipFile);
      return resolve(pathToExportZipFile);
    } catch (cause) {
      reject(cause);
    }
  });
};