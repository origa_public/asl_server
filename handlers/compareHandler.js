const jsdiff = require("diff");

const { find } = require("lodash");
function ensureSame(contents1, contents2) {
  return new Promise(async (resolve, reject) => {
    try {
      contents1 = contents1.replace(/\r?\n|\r/g, "");
      contents2 = contents2.replace(/\r?\n|\r/g, "");

      let lines = jsdiff.diffLines(contents1, contents2, {
        ignoreWhitespace: true,
        newlineIsToken: false
      });

      let notSameLine = find(lines, line => {
        return (
          typeof line.removed !== "undefined" ||
          typeof line.added !== "undefined"
        );
      });

      if (typeof notSameLine !== "undefined")
        return reject("CONTENTS NOT SAME");

      return resolve(true);
    } catch (cause) {
      reject(cause);
    }
  });
}

module.exports = { ensureSame };
