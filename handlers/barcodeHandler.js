const _ = require("lodash");
const bwipjs = require("bwip-js");
var fs = require("fs-extra");
var randomatic = require("randomatic");

function generateBarcode(barCodeData, OutputDirectory) {
  return new Promise((resolve, reject) => {
    try {
      bwipjs.toBuffer(
        {
          bcid: "code128", // Barcode type
          text: barCodeData.toString(), // Text to encode
          scale: 3, // 3x scaling factor
          height: 10, // Bar height, in millimeters
          includetext: true, // Show human-readable text
          textxalign: "center", // Always good to set this
          backgroundcolor: "ffffff",
          paddingheight: 10,
          paddingwidth: 10
        },
        function(err, png) {
          if (err) {
            return reject(err);
          } else {
            fs.writeFileSync(OutputDirectory, png);
            return resolve(true);
          }
        }
      );
    } catch (cause) {
      return reject(cause);
    }
  });
}

function generateBarcodeNumber() {
  var BarcodeNumb = randomatic("A0", 10);
  return BarcodeNumb;
}

module.exports = {
  generateBarcode,
  generateBarcodeNumber
};
