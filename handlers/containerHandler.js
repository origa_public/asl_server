const Docker = require("dockerode");
const tarfs = require("tar-fs");
const fs = require("fs-extra");

function build(dockerFilePath, imageName) {
  return new Promise(async (resolve, reject) => {
    try {
      const docker = new Docker();
      const pack = tarfs.pack(path.join(dockerFilePath));
      let stream = await docker.buildImage(pack, {
        t: imageName
      });
      await new Promise((resolve, reject) => {
        docker.modem.followProgress(stream, (err, res) =>
          err ? reject(err) : resolve(res)
        );
      });
      return resolve("done");
    } catch (cause) {
      reject(cause);
    }
  });
}

function deploy(imageName, HostConfig, environmentVariables) {
  return new Promise(async (resolve, reject) => {
    try {
      const docker = new Docker();
      let Env = environmentVariables.map(envVar => {
        return envVar.Name + "=" + envVar.Value;
      });

      let container = await docker.createContainer({
        Image: imageName,
        Tty: true,
        HostConfig,
        Env
      });

      let stream = await container.start();

      // ONLY IN DEBUG MODE
      container.attach({ stream: true, stdout: true, stderr: true }, function(
        err,
        stream
      ) {
        if (err) return reject(err);
        stream.pipe(process.stdout);
      });

      return resolve(container.id);
    } catch (cause) {
      reject(cause);
    }
  });
}
function stop(id) {
  return new Promise(async (resolve, reject) => {
    try {
      const docker = new Docker();
      let container = docker.getContainer(id);
      if (container.id == "") return reject("invalid container id");

      await container.stop();
      await container.remove();
      return resolve(true);
    } catch (cause) {
      reject(cause);
    }
  });
}

module.exports = { build, deploy, stop };
