const path = require("path");
const fs = require("fs-extra");

function getImportedDocumentsDirPath() {
  return new Promise(async (resolve, reject) => {
    try {
      let rootPath = require("../app").getRootPath();
      let appName = require("../app").getAppName();
      AppName = appName.split(" ").join("");
      let resPath = path.join(
        rootPath,
        "files",
        AppName,
        "imports",
        "documents"
      );
      await fs.ensureDir(resPath);
      return resolve(resPath);
    } catch (cause) {
      reject(cause);
    }
  });
}

function getImportedMediaDirPath() {
  return new Promise(async (resolve, reject) => {
    try {
      let rootPath = require("../app").getRootPath();
      let appName = require("../app").getAppName();
      AppName = appName.split(" ").join("");
      let resPath = path.join(rootPath, "files", AppName, "imports", "media");
      await fs.ensureDir(resPath);
      return resolve(resPath);
    } catch (cause) {
      reject(cause);
    }
  });
}

function getExportedMediaDirPath() {
  return new Promise(async (resolve, reject) => {
    try {
      let rootPath = require("../app").getRootPath();
      let appName = require("../app").getAppName();
      AppName = appName.split(" ").join("");
      let resPath = path.join(rootPath, "files", AppName, "exports", "media");
      await fs.ensureDir(resPath);
      return resolve(resPath);
    } catch (cause) {
      reject(cause);
    }
  });
}

function getExportedZipDirPath() {
  return new Promise(async (resolve, reject) => {
    try {
      let rootPath = require("../app").getRootPath();
      let appName = require("../app").getAppName();
      AppName = appName.split(" ").join("");
      let resPath = path.join(rootPath, "files", AppName, "exports", "zips");
      await fs.ensureDir(resPath);
      return resolve(resPath);
    } catch (cause) {
      reject(cause);
    }
  });
}

function getExportedDocumentDirPath() {
  return new Promise(async (resolve, reject) => {
    try {
      let rootPath = require("../app").getRootPath();
      let appName = require("../app").getAppName();
      AppName = appName.split(" ").join("");
      let resPath = path.join(
        rootPath,
        "files",
        AppName,
        "exports",
        "documents"
      );
      await fs.ensureDir(resPath);
      return resolve(resPath);
    } catch (cause) {
      reject(cause);
    }
  });
}

module.exports = {
  getImportedDocumentsDirPath,
  getImportedMediaDirPath,
  getExportedMediaDirPath,
  getExportedZipDirPath,
  getExportedDocumentDirPath
};
