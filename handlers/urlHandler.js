function getExportMediaUrl(projectName, filename) {
  return new Promise(async (resolve, reject) => {
    try {
      // console.log(projectName);
      let configuration = require("../app").getAppConfiguration();
      let appDetail = require("../app");
      // console.log(configuration);
      let PORT = null;
      if (configuration[projectName].PORT == "80") {
        PORT = "";
      } else {
        PORT = ":" + configuration[projectName].PORT;
      }
      const baseUrl =
        configuration[projectName].PROTOCOL +
        "://" +
        configuration[projectName].HOSTNAME +
        PORT +
        "/" +
        configuration[projectName].MEDIA_FOLDER +
        "/exports/media/" +
        filename;
      //console.log(baseUrl);
      return resolve(baseUrl);
    } catch (cause) {
      return reject(cause);
    }
  });
}

function getImportMediaUrl(projectName, filename) {
  return new Promise(async (resolve, reject) => {
    try {
      // console.log(projectName);
      let configuration = require("../app").getAppConfiguration();
      let appDetail = require("../app");
      // console.log(configuration);
      let PORT = null;
      if (configuration[projectName].PORT == "80") {
        PORT = "";
      } else {
        PORT = ":" + configuration[projectName].PORT;
      }
      const baseUrl =
        configuration[projectName].PROTOCOL +
        "://" +
        configuration[projectName].HOSTNAME +
        PORT +
        "/" +
        configuration[projectName].MEDIA_FOLDER +
        "/imports/media/" +
        filename;
      return resolve(baseUrl);
    } catch (cause) {
      return reject(cause);
    }
  });
}

function getDocumentUrl(projectName, filename) {
  return new Promise(async (resolve, reject) => {
    try {
      // console.log(projectName);
      let configuration = require("../app").getAppConfiguration();
      let appDetail = require("../app");
      // console.log(configuration);
      let PORT = null;
      if (configuration[projectName].PORT == "80") {
        PORT = "";
      } else {
        PORT = ":" + configuration[projectName].PORT;
      }
      const baseUrl =
        configuration[projectName].PROTOCOL +
        "://" +
        configuration[projectName].HOSTNAME +
        PORT +
        "/" +
        configuration[projectName].MEDIA_FOLDER +
        "/imports/documents/" +
        filename;
      return resolve(baseUrl);
    } catch (cause) {
      return reject(cause);
    }
  });
}

module.exports = { getExportMediaUrl, getImportMediaUrl, getDocumentUrl };

// const baseUrl =
//   configuration.userManagement.PROTOCOL +
//   "://" +
//   configuration.userManagement.HOSTNAME +
//   ":" +
//   configuration.userManagement.PORT +
//   "/USER_MANAGEMENT";
// axios.defaults.baseURL = baseUrl;
// let userApiRes = await axios.get("/user/checkExists", {
//   params: {
//     _id: userId
//   }
// });

// if (userApiRes.data.statusCode == 0) {
//   return resolve(true);
// }
