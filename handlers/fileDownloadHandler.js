const fs = require("fs-extra");
const Path = require("path");
const Axios = require('axios');


async function getFile(videoUrl, desiredVideoUrl) {
    try {
        const url = videoUrl;
        
        const path = Path.resolve(__dirname, '..', 'files', 'imports', 'media', desiredVideoUrl);
        
        const writer = fs.createWriteStream(path)
    
        const response = await Axios({
            url,
            method: 'GET',
            responseType: 'stream'
        })
    
        response.data.pipe(writer)
    
        return new Promise((resolve, reject) => {
            writer.on('finish', resolve)
            writer.on('error', reject)
        });
    } catch(cause) {
        console.log(cause);
    }
}

module.exports = {
    getFile
}