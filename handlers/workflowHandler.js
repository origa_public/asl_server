const ejs = require("ejs");
const fileHandler = require("./fileHandler");
const { join } = require("path");
const { forEach } = require("lodash");
const { Parser } = require("json2csv");

function getCardDesignFromConfig(CardConfig) {
  return new Promise(async (resolve, reject) => {
    try {
      let cardDesignerEjsPath = join(
        __dirname,
        "..",
        "ejs_templates",
        "cardDesigner.ejs"
      );
      let cardDesignerContents = await fileHandler.readFileContents(
        cardDesignerEjsPath
      );
      let cardDesignContentsStr = cardDesignerContents.toString();
      let template = ejs.compile(cardDesignContentsStr);
      let result = template({ CardConfig });
      return resolve(result);
    } catch (cause) {
      reject(cause);
    }
  });
}

function getWorkflowDesign(WorkflowName, WorkflowId, Menu) {
  return new Promise(async (resolve, reject) => {
    try {
      let workflowDesignerEjsPath = join(
        __dirname,
        "..",
        "ejs_templates",
        "workflowDesigner.ejs"
      );
      let workflowDesignerContents = await fileHandler.readFileContents(
        workflowDesignerEjsPath
      );
      let workflowDesignContentsStr = workflowDesignerContents.toString();
      let template = ejs.compile(workflowDesignContentsStr);
      let result = template({ Workflow: { WorkflowName, WorkflowId, Menu } });
      return resolve(result);
    } catch (cause) {
      reject(cause);
    }
  });
}

function generateExcel(excelFilePath, workflowConfig) {
  return new Promise(async (resolve, reject) => {
    try {
      let excelColumns = new Set();
      let { Cards } = workflowConfig;
      forEach(Cards, Card => {
        let { Inputs } = Card;
        forEach(Inputs, Input => {
          let { Value } = Input;
          if (!Value.includes("$")) return;
          let words = Value.split(" ");
          forEach(words, word => {
            if (!word.includes("$")) return;
            let excelColumn = word.substr(1, word.length);
            excelColumns.add(excelColumn);
          });
        });
      });
      let excelColumnsArray = Array.from(excelColumns);
      const opts = { excelColumnsArray };
      const parser = new Parser(opts);
      let data = [{ PARA1: " ", PARA2: " ", PARA3: " ", PARA4: " " }];
      const csv = parser.parse(data);
      await fileHandler.writeFileContents(excelFilePath, csv);
      return resolve(true);
    } catch (cause) {
      reject(cause);
    }
  });
}

function getInputColumns(workflowConfig) {
  return new Promise(async (resolve, reject) => {
    try {
      let inputColumns = [];
      let inputColumnsSet = new Set();
      let { Cards } = workflowConfig;
      forEach(Cards, Card => {
        let { Inputs } = Card;
        forEach(Inputs, Input => {
          let { Value } = Input;
          let words = Value.split(" ");
          forEach(words, word => {
            if (!word.includes("$")) return;
            let inputColumn = word.substr(1, word.length);
            inputColumnsSet.add(inputColumn);
          });
        });
      });
      inputColumns = Array.from(inputColumnsSet);
      return resolve(inputColumns);
    } catch (cause) {
      reject(cause);
    }
  });
}

function getOutputColumns(workflowConfig) {
  return new Promise(async (resolve, reject) => {
    try {
      let outputColumns = [];
      let outputColumnsSet = new Set();
      let { Cards } = workflowConfig;
      forEach(Cards, Card => {
        let { Outputs } = Card;
        forEach(Outputs, Output => {
          let { Value } = Output;
          let words = Value.split(" ");
          forEach(words, word => {
            outputColumnsSet.add(word);
          });
        });
      });
      outputColumns = Array.from(outputColumnsSet);
      return resolve(outputColumns);
    } catch (cause) {
      reject(cause);
    }
  });
}
//
///////////////////////////
// let workflowConfig = {
//   Cards: [
//     {
//       CardNumber: "1562924410270",
//       CardName: "END",
//       Navigations: [],
//       Inputs: [],
//       Outputs: []
//     },
//     {
//       CardNumber: "1562924412509",
//       CardName: "START",
//       Navigations: [{ From: "OnStart", To: "1563535744863" }],
//       Inputs: [
//         { Name: "WorkflowName", Value: "W1" },
//         { Name: "Menu", Value: "MENU" }
//       ],
//       Outputs: []
//     },
//     {
//       CardNumber: "1562924501655",
//       CardName: "CARD",
//       Navigations: [
//         { From: "OnPass", To: "1562924410270" },
//         { From: "OnPass", To: "1562924410270" }
//       ],
//       Inputs: [
//         { Name: "PARA1", Value: "$PARA1 IS OK" },
//         { Name: "PARA2", Value: "$PARA2  IS THE MAIN  $PARA3" }
//       ],
//       Outputs: [{ Name: "OUT1", Value: "3" }, { Name: "OUT2", Value: "4" }]
//     },
//     {
//       CardNumber: "1563535744863",
//       CardName: "CARD",
//       Navigations: [
//         { From: "OnPass", To: "1562924501655" },
//         { From: "OnPass", To: "1562924501655" }
//       ],
//       Inputs: [
//         { Name: "PARA1", Value: "$PARA1 IS THE THING" },
//         { Name: "PARA2", Value: "$PARA4" }
//       ],
//       Outputs: [{ Name: "OUT1", Value: "" }, { Name: "OUT2", Value: "" }]
//     }
//   ]
// };

///////////////////
module.exports = {
  getCardDesignFromConfig,
  getWorkflowDesign,
  generateExcel,
  getInputColumns,
  getOutputColumns
};
