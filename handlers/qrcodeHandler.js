const pathHandler = require("../handlers/pathHandler");
var QRCode = require("qrcode");
var Promise = require("bluebird");
var path = require("path");
var fs = require("fs-extra");

exports.getQRCodeFilePath = function(contents, qrName, pathToSave) {
  return new Promise(async (resolve, reject) => {
    fs.ensureDirSync(pathToSave);
    QRCode.toFile(
      pathToSave + "/" + qrName + ".png",
      contents.toString(),
      {
        width: 512,
        height: 512
      },
      function(err) {
        if (err) {
          return reject(err);
        }
        return resolve(path.join(pathToSave));
      }
    );
  });
};
