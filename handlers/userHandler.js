const axios = require("axios");
var mongoose = require("mongoose");
var jwt = require("json-web-token");
axios.defaults.headers.token =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJVc2VySWQiOiJNQVNURVJfS0VZIn0.d8yulsOUd0HI_p3DCBqvfzt6AD_R52gHxicBAimHSb8";
function checkExistsById(userId) {
  return new Promise(async (resolve, reject) => {
    try {
      let configuration = require("../app").getAppConfiguration();
      const baseUrl =
        configuration.userManagement.PROTOCOL +
        "://" +
        configuration.userManagement.HOSTNAME +
        ":" +
        configuration.userManagement.PORT +
        "/api/USER_MANAGEMENT";
      axios.defaults.baseURL = baseUrl;
      let userApiRes = await axios.get("/user/checkExists", {
        params: {
          _id: userId
        }
      });

      if (userApiRes.data.statusCode == 0) {
        return resolve(true);
      }
      return reject("User not found");
    } catch (cause) {
      return reject(cause);
    }
  });
}

function validateToken(token) {
  return new Promise(async (resolve, reject) => {
    try {
      let secret = "origa18";
      jwt.decode(secret, token, function(err, decodedPayload, decodedHeader) {
        if (err) {
          return reject(err.message);
        } else {
          return resolve(decodedPayload);
        }
      });
      // return resolve(true);
    } catch (cause) {
      return reject(cause);
    }
  });
}

module.exports = { checkExistsById, validateToken };
