const fs = require("fs-extra");
const Promise = require("bluebird");
const statusHandler = require("../handlers/statusHandler");

function readFileContents(filePath) {
  return new Promise(async (resolve, reject) => {
    try {
      if ((await fs.pathExists(filePath)) == false)
        return reject(
          statusHandler.invalidParameteresMsg(
            "no such file named " + filePath + " exists"
          )
        );

      fs.readFile(filePath, (err, data) => {
        if (err) return reject(err);
        return resolve(data.toString());
      });
    } catch (cause) {
      reject(cause);
    }
  });
}

function writeFileContents(filePath, contents) {
  return new Promise(async (resolve, reject) => {
    try {
      await fs.ensureFile(filePath);
      fs.writeFile(filePath, contents, err => {
        if (err) return reject(err);
        return resolve(true);
      });
    } catch (cause) {
      reject(cause);
    }
  });
}

function readJsonContents(filePath) {
  return new Promise(async (resolve, reject) => {
    try {
      if ((await fs.pathExists(filePath)) == false)
        return reject(
          statusHandler.invalidParameteresMsg(
            "no such file named " + filePath + " exists"
          )
        );
      var fileContents = await fs.readJson(filePath);
      return resolve(fileContents);
    } catch (cause) {
      reject(cause);
    }
  });
}

function moveFileTo(file, dstPath) {
  return new Promise(async (resolve, reject) => {
    try {
      if (typeof file === "undefined") return reject("file required");

      if (typeof file.mv === "undefined") return reject("file is not correct ");

      await file.mv(dstPath);

      return resolve(true);
    } catch (cause) {
      reject(cause);
    }
  });
}

module.exports = {
  readFileContents,
  readJsonContents,
  moveFileTo,
  writeFileContents
};
