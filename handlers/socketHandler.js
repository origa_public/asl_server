function sendToClient(topic, message) {
  return new Promise(async (resolve, reject) => {
    try {
      //console.log("TOPIC ---->" + topic + " MESSAGE ---->" + message);
      const { io } = require("../app");
      // FOR NON RELAY SERVER FOR BACKWARD COMPAT
      io().emit(topic.toUpperCase(), message);
      let dataSendToRelay = { topic: topic.toUpperCase(), message };
      // FOR RELAY SERVER FORWARD
      io().emit("EVENT", dataSendToRelay);
      return resolve(true);
    } catch (cause) {
      reject(cause);
    }
  });
}

module.exports = {
  sendToClient
};
