const fs = require("fs-extra");
const path = require("path");
const mongoose = require("mongoose");
const express = require("express");
let router = express.Router();

fs.readJSON(path.join(__dirname, "package.json"), (err, packageJson) => {
  console.log("*************************************************");
  console.log("using asl_server version ---> " + packageJson.version);
  console.log("*************************************************");
  console.log("");
});

// APP
const app = require("./app");

// HANDLERS
// let exportedContents = {};
// let handlers = fs.readdirSync(path.join(__dirname, "handlers"));
// handlers.map(handler => {
//   let handlerPath = path.join(__dirname, "handlers", handler);
//   let handlerName = handler.substr(0, handler.length - 3);
//   exportedContents[handlerName] = require(handlerPath);
// });

//for(let handlerIndex =0;)
//console.log(handlers);

const barcodeHandler = require("./handlers/barcodeHandler");
const containerHandler = require("./handlers/containerHandler");
const errorHandler = require("./handlers/errorHandler");
const fileDownloadHandler = require("./handlers/fileDownloadHandler");
const fileHandler = require("./handlers/fileHandler");
const jsonHandler = require("./handlers/jsonHandler");
const mediaHandler = require("./handlers/mediaHandler");
const parameterHandler = require("./handlers/parameterHandler");
const pathHandler = require("./handlers/pathHandler");
const qrcodeHandler = require("./handlers/qrcodeHandler");
const socketHandler = require("./handlers/socketHandler");
const sessionHandler = require("./handlers/sessionHandler");
const statusHandler = require("./handlers/statusHandler");
const variableHandler = require("./handlers/variableHandler");
const zipHandler = require("./handlers/zipHandler");
const workflowHandler = require("./handlers/workflowHandler");
const userHandler = require("./handlers/userHandler");
const urlHandler = require("./handlers/urlHandler");

// CONTROLLER
const controller = require("./controllers/controller");

// MODEL
const Model = require("./models/model");

// exportedContents = Object.assign(exportedContents, {
//   controller,
//   app,
//   Model,
//   mongoose,
//   router
// });

//console.log(Object.getOwnPropertyNames(exportedContents));

module.exports = {
  barcodeHandler,
  containerHandler,
  fileDownloadHandler,
  fileHandler,
  mediaHandler,
  pathHandler,
  qrcodeHandler,
  sessionHandler,
  zipHandler,
  errorHandler,
  jsonHandler,
  parameterHandler,
  statusHandler,
  variableHandler,
  socketHandler,
  workflowHandler,
  userHandler,
  urlHandler,
  controller,
  app,
  Model,
  mongoose,
  router
};

// const jsdiff = require("diff");
// let A = "PIYUSH ARORA ";
// let B = "PIYUSH ARORA ";

// console.log(jsdiff(A, B));
