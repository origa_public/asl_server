FROM node:10

WORKDIR /app

COPY ./package.json /app

RUN npm i

RUN rm /app/package.json

RUN npm install git+https://git@gitlab.com/origa_public/asl_server

RUN rm /app/package-lock.json
