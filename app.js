const express = require("express");
const path = require("path");
const cors = require("cors");
const bodyParser = require("body-parser");
const fileUpload = require("express-fileupload");
const winston = require("winston");
const expressWinston = require("express-winston");
const errorHandler = require("./handlers/errorHandler");
const statusHandler = require("./handlers/statusHandler");
const userHandler = require("./handlers/userHandler");
const mongoose = require("mongoose");
const session = require("express-session");
const connectMongo = require("connect-mongo");
const MongoStore = connectMongo(session);

const addRequestId = require("express-request-id")();
const app = express();
const http = require("http").Server(app);
const socket_io = require("socket.io");

let io = null;
let serverInstance = null;
let rootPath = null;
let appName = null;
let appConfiguration = null;
let configuration = null;

function connectToDb(database) {
  return new Promise(async (resolve, reject) => {
    try {
      mongoose
        .connect(database.MONGO_URL, { useNewUrlParser: true })
        .catch(() => {});
      let db = mongoose.connection;
      db.on("connected", () => {
        console.log(
          "successfully connected to db  ====> " + database.MONGO_URL
        );
        return resolve(true);
      });
      db.on("open", () => {
        return resolve(true);
      });
      db.on("close", () => {
        console.log("close");
      });
      db.on("connecting", () => {
        console.log("connecting to db ... ");
      });
      db.on("disconnected", () => {
        setTimeout(() => {
          mongoose
            .connect(database.MONGO_URL, { useNewUrlParser: true })
            .catch(() => {});
        }, 5000);
      });
      db.on("reconnected", () => {
        console.log("reconnected to db");
        return resolve(true);
      });
      db.on("error", err => {
        console.log("got error connecting db ");
        console.log(err);
        setTimeout(() => {
          mongoose
            .connect(database.MONGO_URL, {
              useNewUrlParser: true,
              useUnifiedTopology: true
            })
            .catch(() => {});
        }, 5000);
      });
    } catch (cause) {
      console.log("cannot handle error");
      console.log(cause);
      process.exit();
    }
  });
}

function start(config, appConfig, projectRootPath) {
  return new Promise(async (resolve, reject) => {
    try {
      // INIT GLOBAL VARIABLES
      rootPath = projectRootPath;
      appName = appConfig.NAME;
      configuration = config;
      appConfiguration = config;
      // CREATE SOCKETIO SERVER
      io = socket_io(http, {
        origins: "*:*",
        path: "/socket/" + appName
      });

      // TODO
      app.use(cors());
      app.use(express.static(path.join(rootPath, "files")));

      await connectToDb(config.database);

      mongoose.Promise = global.Promise;
      mongoose.set("useCreateIndex", true);
      mongoose.set("useFindAndModify", false);
      const db = mongoose.connection;

      app.use(
        session({
          secret: "my-secret",
          resave: false,
          saveUninitialized: true,
          store: new MongoStore({
            mongooseConnection: db
          })
        })
      );

      // CONFIGURE BODY PARSER
      app.use(bodyParser.json());
      app.use(
        bodyParser.urlencoded({
          extended: false
        })
      );
      app.use(
        fileUpload({
          limits: {
            fileSize: 500 * 1024 * 1024,
            safeFileNames: true
          }
        })
      );

      // CONFIGURE REQUEST ID
      app.use(addRequestId);

      // CUSTOM MIDDLEWARE FOR AUTHENTICATION
      app.use(async (req, res, next) => {
        try {
          let rootPath = req.path.split("/")[1];

          if (rootPath == appName) {
            //console.log("i am here");
            next();
          } else if (
            req.path == "/api/USER_MANAGEMENT/user/create" ||
            req.path == "/api/USER_MANAGEMENT/user/login" ||
            req.path == "/api/USER_MANAGEMENT/userGroup/create" ||
            req.path == "/api/USER_MANAGEMENT/userGroup/uploadConfig" ||
            req.path == "/api/USER_MANAGEMENT/userGroup/fetch"
          ) {
            next();
          } else {
            //console.log("PANGA.........");
            let token = req.headers.token;
            if (typeof token == "undefined") {
              // console.log("Please set a token");

              return res.json(
                statusHandler.invalidParameteresMsg("Token not found")
              );
            } else {
              let userDetail = await userHandler.validateToken(token);
              if (userDetail.UserId == "MASTER_KEY") {
                next();
              } else {
                await userHandler.checkExistsById(userDetail.UserId);
                req.UserId = userDetail.UserId;
                next();
              }
            }
          }
        } catch (cause) {
          return res.json(
            statusHandler.invalidParameteresMsg("InValid Token found")
          );
        }
      });

      let AppName = appName.split(" ").join("");
      let resPath = path.join(rootPath, "files", AppName, "logs");

      //CONFIGURE MODELS
      for (
        var modelIndex = 0;
        modelIndex < appConfig.MODELS.length;
        modelIndex++
      ) {
        //console.log("haha");
        let modelName = appConfig.MODELS[modelIndex].NAME;
        let modelFileName =
          modelName
            .toString()
            .charAt(0)
            .toLowerCase() +
          modelName.slice(1) +
          "Model";
        let modelFilePath = path.join(projectRootPath, "models", modelFileName);
        let modelClass = require(modelFilePath);
        new modelClass(modelName).get();
      }

      // CONFIGURE ROUTES
      for (
        var routeIndex = 0;
        routeIndex < appConfig.MODELS.length;
        routeIndex++
      ) {
        let routeName =
          appConfig.MODELS[routeIndex].NAME.toString()
            .charAt(0)
            .toLowerCase() +
          appConfig.MODELS[routeIndex].NAME.toString().slice(1);
        let routeFileName =
          routeName
            .toString()
            .charAt(0)
            .toLowerCase() +
          routeName.slice(1) +
          "Route";
        let routeFilePath = path.join(projectRootPath, "routes", routeFileName);
        let route = require(routeFilePath);
        app.use("/api/" + appName + "/" + routeName, route);
      }

      app.use(
        expressWinston.logger({
          transports: [
            new winston.transports.File({
              filename: resPath + "/requestLog.json"
            })
          ],
          meta: false, // optional: control whether you want to log the meta data about the request (default to true)
          msg:
            "HTTP {{req.method}} {{new Date()}} {{req.url}} {{JSON.stringify(req.body)}}",
          expressFormat: false, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
          colorize: true, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
          ignoreRoute: function(req, res) {
            return false;
          } // optional: allows to skip some log messages based on request and/or response
        })
      );

      app.get(/.*/, (req, res) => {
        res.json(statusHandler.entryNotFoundMsg("INVALID ROUTE"));
        //res.sendFile(__dirname + "/public/index.html");
      });

      //CUSTOM EXPRESS ERROR HANDLER
      app.use(async (err, req, res, next) => {
        var msg = await errorHandler.handle(err);
        //config.HANDLE_ERROR(err);
        console.log(err);
        res.json(msg);
      });

      // //START WEBSERBER
      serverInstance = http.listen(config.host.PORT, () => {
        console.log(
          appConfig.NAME +
            " webserver started at port number " +
            config.host.PORT
        );
        resolve("done");
      });
    } catch (cause) {
      console.log("error 2-------------");
      reject(cause);
    }
  });
}

async function stopWebserver() {
  return new Promise(async (resolve, reject) => {
    try {
      if (serverInstance == null) return reject("no server instance");
      serverInstance.close();
      mongoose.disconnect();

      return resolve("done");
    } catch (cause) {
      reject(cause);
    }
  });
}

function getDb() {
  //console.log(mongoose.connection.db);
  return mongoose.connection.db;
}

function getRootPath() {
  return rootPath;
}

function getAppName() {
  return appName;
}

function getAppConfig() {
  return appConfiguration;
}

function getAppConfiguration() {
  return configuration;
}

function getIo() {
  return io;
}

module.exports = {
  start,
  stopWebserver,
  io: getIo,
  db: getDb,
  getRootPath,
  getAppName,
  getAppConfig,
  getAppConfiguration
};
