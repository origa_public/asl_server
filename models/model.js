const mongoose = require("mongoose");

//var Page = mongoose.model("Page", PageSchema);
//module.exports = Page;

class Model {
  constructor(ModelName, Properies) {
    let CommonProperites = {
      LastUpdatedOn: {
        type: Date,
        require: true,
        default: Date.now
      },
      CreatedOn: {
        type: Date,
        default: new Date()
      },
      IsDeleted: {
        type: Boolean,
        default: false
      }
    };

    const combinedProperties = Object.assign(CommonProperites, Properies);
    const combinedSchema = mongoose.Schema(combinedProperties);
    this.Model = mongoose.model(ModelName, combinedSchema);
  }

  get() {
    return this.Model;
  }
}

module.exports = Model;
